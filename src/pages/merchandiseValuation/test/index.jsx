import React, { useState, useEffect, useRef } from 'react';
import { Radio, Row, Col, Form, Select, Button } from 'antd';
import { withRouter } from 'react-router';
import apiService from '@/api/apis';
import MyTable from '../compontents/table';
const App = ({}) => {
  const [form] = Form.useForm();
  const [flagData, setFlagData] = useState([]); //类型
  const [flag, setFlag] = useState(); //当前选中的类型
  const [seriesData, setSeriesData] = useState([]); //系列
  const [params, setParams] = useState({}); //搜素筛选
  const [currentTab, setCurrentTab] = useState(0); //产品tab
  const [productData, setProductData] = useState([
    { label: 'tab1', value: 0 },
    { label: 'tab2', value: 1 },
    { label: 'tab3', value: 2 },
  ]);

  useEffect(() => {
    listEcsflag();
  }, []);
  useEffect(() => {
    flag && listEcsSeries();
  }, [flag]);
  //类型
  const listFlag = () => {
    apiService.listFlag().then((res) => {
      let arr = res.data || [];
      arr.map((item, index) => {
        arr[index] = { label: `${item}`, value: item };
      });
      form.setFieldsValue({ flag: arr[0].value });
      setFlagData(arr || []);
      setFlag(arr[0].value || '');
    });
  };
  //系列
  const listSeries = () => {
    apiService.listSeries({ flag }).then((res) => {
      setSeriesData(res.data || []);
    });
  };
  return (
    <div className="tablePage">
      <Form name="basic" form={form}>
        <Row gutter={[40]}>
          <Col span={6}>
            <Form.Item label="产品">
              <Radio.Group
                defaultValue={0}
                onChange={(e) => {
                  setCurrentTab(e.target.value);
                }}
              >
                {productData.map((item, index) => {
                  return <Radio.Button value={item.value}>{item.label}</Radio.Button>;
                })}
              </Radio.Group>
            </Form.Item>
          </Col>
          {currentTab == 0 && (
            <>
              <Col span={6}>
                <Form.Item name="flag" label="类型">
                  <Radio.Group
                    onChange={(e) => {
                      setFlag(e.target.value);
                      form.setFieldsValue({ instanceTypeFamily: undefined });
                    }}
                  >
                    {flagData.map((item, index) => {
                      return <Radio.Button value={item.value}>{item.label}</Radio.Button>;
                    })}
                  </Radio.Group>
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item label="系列" name="instanceTypeFamily">
                  <Select
                    placeholder="请选择"
                    onChange={(val) => {
                      setParams({ instanceTypeFamily: val, pageNum: 1 });
                    }}
                    allowClear
                  >
                    {seriesData.map((item, index) => {
                      return <Option value={item}>{item}</Option>;
                    })}
                  </Select>
                </Form.Item>
              </Col>
            </>
          )}
        </Row>
      </Form>
      {currentTab == 0 && <MyTable name="ECSInstance" params={params} />}
    </div>
  );
};

export default withRouter(App);
