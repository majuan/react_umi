import React, { useState, useEffect, useRef } from 'react';
import { Tabs } from 'antd';
import { withRouter } from 'react-router';
import Test from './test';

const { TabPane } = Tabs;
const OrderTemplateRoles = () => {
  const [currentTab, setCurrentTab] = useState(6);
  const [tabsData, setTabsData] = useState([{ label: 'Test', key: 'test', value: 0 }]);
  useEffect(() => {}, []);

  return (
    <div className="tablePage">
      <Tabs
        defaultActiveKey={currentTab}
        onChange={(val) => {
          setCurrentTab(val);
        }}
      >
        {tabsData.map((item) => (
          <TabPane tab={item.label} key={item.value}></TabPane>
        ))}
      </Tabs>
      {currentTab == '0' && <Test />}
    </div>
  );
};

export default withRouter(OrderTemplateRoles);
