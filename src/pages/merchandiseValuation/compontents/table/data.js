let rules = [
  {
    required: true,
    message: '请输入',
  },
  {
    pattern: /^[1-9]\d{0,4}(\.\d{1,6})?$|^0(\.\d{1,6})?$/,
    message: '请输入',
  },
];

export const getData = (name, rowData) => {
  const data = [
    {
      url: '页面路径',
      tableName: 'testTableName',
      //请求的接口路径，在api里面设置的
      requestUrl: {
        listUrl: 'list', //列表
        detailUrl: 'getDetail', //详情
        editUrl: 'update', //修改
        stopStartUrl: 'changeStatusFor', //上下架、停用启用
        keyId: 'PriceId', //业务主键
      },
      pagination: true,//是否展示分页
      columns: [
        {
          title: '规格',
          dataIndex: 'guige',
          fixed: 'left',
        },
        {
          title: '描述',
          dataIndex: 'miaoshu',
        },
      ],
      FormItems: [
        {
          type: 'Text',
          label: '产品',
          render: (record) => '产品',
        },
        {
          type: 'Text',
          label: '数量',
          name: 'num',
          suffix: '个',
        },
        {
          type: 'Input',
          label: '单价',
          name: 'price',
          rules,
          props: {
            maxLength: 12,
            suffix: '元/个',
          },
        },
      ],
    },
  ];

  let table = data.filter((item) => item.tableName == name)[0];
  table.columns.map((item, index) => {
    table.columns[index] = {
      ellipsis: true,
      render: (text) => <span>{text || '-'}</span>,
      ...item,
    };
  });
  return table;
};
