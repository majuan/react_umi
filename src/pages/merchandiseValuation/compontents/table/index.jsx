import React, { useState, useEffect, useRef } from 'react';
import { Table, Popconfirm } from 'antd';
import { withRouter } from 'react-router';
import apiService from '@/api/apis';
import AuthWrapper from '@/components/AuthWrapper';
import MyForm from '@/components/form/form';
import { getData } from './data';
const App = ({ name, params = {}, submitParams = {} }) => {
  const [cmp, setCmp] = useState(getData(name));
  let {
    pagination = false,
    columns,
    title,
    formProps = {},
    controlsProps = {},
    operate = { edit: { btnText: '修改', title: '修改商品定价' } },
    requestUrl: { listUrl, detailUrl, editUrl, addUrl, deleteUrl, keyId, stopStartUrl },
  } = cmp;

  const editFormRef = useRef();
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false); //table lodaing
  const [rowData, setRowData] = useState({}); //详情数据
  const [FormItems, setFormItems] = useState(cmp.FormItems); //详情数据
  const [tableParams, setTableParams] = useState(
    pagination ? { pageNum: 1, pageSize: 10, ...params } : { ...params },
  );
  const [total, setTotal] = useState(0);
  useEffect(() => {
    getList();
  }, [tableParams]);
  useEffect(() => {
    if (JSON.stringify(params) !== '{}') {
      setTableParams(Object.assign({}, tableParams, { ...params }));
    }
  }, [params]);
  const getList = () => {
    setLoading(true);
    apiService[listUrl](tableParams)
      .then((res) => {
        if (pagination) {
          setData(res.data.rows || []);
          setTotal(res.data.total);
        } else {
          setData(res.data || []);
        }
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
      });
  };

  //获取详情
  const getDetail = (id) => {
    apiService[detailUrl]({ [keyId]: id }).then((res) => {
      setRowData(res.data || []);
      setFormItems(getData(name, res.data).FormItems);
    });
  };

  const handleControl = (params, url, action) => {
    apiService[url](params)
      .then((res) => {
        (action == 'edit' || action == 'add') && editFormRef.current.setVisible(false);
        getList();
      })
      .catch(() => {
        (action == 'edit' || action == 'add') && editFormRef.current.setLoading(false);
      });
  };

  let controls = {
    title: '操作',
    width: 100,
    fixed: 'right',
    render: (text, record) => {
      return (
        <AuthWrapper functionName="PORTAL_USER_MANEGE_OAN">
          <MyForm
            modalData={{
              clickNode: <span className="blue">{operate.edit.btnText}</span>,
              title: operate.edit.title,
            }}
            FormItems={FormItems}
            cRef={editFormRef}
            hasSearchBtn={false}
            showModal={true}
            ColSpan={24}
            initialValues={rowData}
            handleShowModal={(visible) => {
              if (visible) {
                getDetail(record[keyId]);
              }
            }}
            onFinish={(values) => {
              handleControl(
                {
                  ...values,
                  ...submitParams,
                  [keyId]: record[keyId],
                },
                editUrl,
                'edit',
              );
            }}
            {...formProps}
          />
          {stopStartUrl && (
            <>
              <Popconfirm
                placement="topRight"
                title={
                  <div>
                    <div className="cl85">{`确定要下架改数据吗？`}</div>
                  </div>
                }
                onConfirm={() =>
                  handleControl({ [keyId]: record[keyId], status: false }, stopStartUrl)
                }
              >
                {record.status && <span className="blue">下架</span>}
              </Popconfirm>
              {!record.status && (
                <span
                  className="blue"
                  onClick={() =>
                    handleControl({ [keyId]: record[keyId], status: true }, stopStartUrl)
                  }
                >
                  上架
                </span>
              )}
            </>
          )}
          {operate.delete && (
            <Popconfirm
              placement="topRight"
              title={
                <div>
                  <div className="cl85">{`确定要删除该数据吗？`}</div>
                </div>
              }
              onConfirm={() => handleControl({ [keyId]: record[keyId] }, deleteUrl)}
            >
              {record.isOperate == true && <span className="blue">删除</span>}
            </Popconfirm>
          )}
        </AuthWrapper>
      );
    },
    ...controlsProps,
  };
  const insert = (arr, item, index) => {
    var a = arr.slice(0);
    a.splice(index, 0, item);
    return a;
  };
  columns = insert(columns, controls, columns.length);
  return (
    <div className="tablePage" style={{ marginBottom: '24px' }}>
      <div className="titleBox">
        {title && <div className="title">{title}</div>}
        {operate.add && (
          <AuthWrapper functionName="PORTAL_ROLE_MANEGE_OAN">
            <MyForm
              cRef={editFormRef}
              showModal={true}
              ColSpan={24}
              modalData={{ btnText: operate.add.btnText, title: operate.add.title }}
              FormItems={FormItems}
              onFinish={(values) => {
                handleControl({ ...values, ...submitParams }, addUrl, 'add');
              }}
              {...formProps}
            />
          </AuthWrapper>
        )}
      </div>

      <Table
        dataSource={data}
        loading={loading}
        columns={columns}
        scroll={controlsProps && controlsProps.fixed == false ? {} : { x: 1300 }}
        pagination={
          pagination
            ? {
                current: tableParams.pageNum,
                pageSize: tableParams.pageSize,
                total,
                pageSizeOptions: [10, 20, 50],
                showTotal: (total) => `共有 ${total} 条`,
                showSizeChanger: true,
                showQuickJumper: true,
                onChange: (page, pageSize) =>
                  setTableParams(
                    Object.assign({}, tableParams, { pageNum: page, pageSize: pageSize }),
                  ),
              }
            : false
        }
      />
    </div>
  );
};

export default withRouter(App);
