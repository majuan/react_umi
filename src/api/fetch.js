import axios from 'axios';
import { message, Modal } from 'antd';
import { removeAllCookie } from '@/utils';
import { history } from 'umi';
import OSS from 'ali-oss';
import { set, get } from '../utils/index';
const baseUrl = process.env.NODE_ENV === 'development' ? '' : '';
const service = axios.create({ baseURL: baseUrl });
// let OSS = window.OSS.Wrapper;
let client = new OSS({
  region: 'oss-cn-hangzhou',
  accessKeyId: 'LTAI4G2qiKhdYXQVTnPST5c9',
  accessKeySecret: 'UjXiInWnGsj8Gx56oUifFD4IhAGKlt',
  secure: true,
  bucket: 'odc-test', //自定义的上传后地址，加在oss前
});
window.client = client;
class Http {
  uploadFile(file, val, prefix, dataOss, datasearchValue) {
    let client = new OSS({
      region: 'oss-cn-hangzhou',
      accessKeyId:
        dataOss && dataOss.accessKeyId ? dataOss.accessKeyId : 'LTAI4G2qiKhdYXQVTnPST5c9',
      accessKeySecret:
        dataOss && dataOss.accessKeySecret
          ? dataOss.accessKeySecret
          : 'UjXiInWnGsj8Gx56oUifFD4IhAGKlt',
      secure: true,
      bucket: dataOss ? dataOss.bucketName : 'odc-test', //自定义的上传后地址，加在oss前
      cname: dataOss ? true : false,
      endpoint: dataOss ? dataOss.bucketName + '.' + dataOss.endPoint : '',
    });
    let datasearchValueType = datasearchValue ? datasearchValue : '';
    let storeAs = dataOss
      ? datasearchValueType + prefix + val
      : 'file/' + new Date() * 1 + '/' + val.replace(/[:*|'"!@#$%&<>?\/\\]/g, '');
    return client
      .multipartUpload(storeAs, file)
      .then((result) => {
        let obj = {};
        obj.nohand = true;
        obj.key = result.name;
        obj.name = result.name;
        obj.uid = result.name;
        obj.url = client.signatureUrl(result.name);
        return Promise.resolve(obj);
      })
      .catch((error) => {
        let obj = {
          ...error,
          nohand: false,
        };
        return Promise.resolve(obj);
      });
  }
  /* eslint-disable */
  get(url, params) {
    // GET请求
    const newUrl = params ? this.build(url, params) : url;
    return this.request(newUrl, {
      method: 'GET',
    });
  }

  post(url, body, hasMessage, hasLoading, responseType, fileBol) {
    // POST请求
    let options = {
      method: 'POST',
    };
    if (body) {
      if (fileBol) {
        options.body = body;
      } else {
        options.body = JSON.stringify(body);
      }
    }
    return this.request(url, options, hasMessage, hasLoading, responseType);
  }
  put(url, body) {
    // PUT请求
    let options = {
      method: 'PUT',
    };
    if (body) options.body = JSON.stringify(body);
    return this.request(url, options);
  }
  delete(url, body) {
    // DELETE请求
    let options = {
      method: 'DELETE',
    };
    if (body) options.body = JSON.stringify(body);
    return this.request(url, options);
  }
  patch(url, body) {
    // PATCH请求
    let options = {
      method: 'patch',
    };
    if (body) options.body = JSON.stringify(body);
    return this.request(url, options);
  }
  downloadFileGet(url, params) {
    // 下载文件
    const newUrl = params ? this.build(url, params) : url;
    axios({
      method: 'get',
      url: url,
      // data: params,
      responseType: 'blob',
    })
      .then((resp) => {
        let headers = resp.headers;
        let contentType = headers['content-type'];
        if (!resp.data) {
          return false;
        } else {
          const blob = new Blob([resp.data], { type: contentType });
          const contentDisposition = resp.headers['content-disposition'];
          let fileName = params;
          if (contentDisposition) {
            fileName = window.decodeURIComponent(
              window.decodeURI(resp.headers['content-disposition'].split('=')[1]),
            );
          }
          this.downFile(blob, fileName);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  downloadFilePost(url, params) {
    // 下载文件
    axios
      .post(url, { ...params }, { responseType: 'blob' })
      .then((resp) => {
        let headers = resp.headers;
        let contentType = headers['content-type'];
        let that = this;
        let reader = new FileReader();
        reader.readAsText(resp.data, 'utf-8');
        reader.onload = function () {
          try {
            if (JSON.parse(reader.result).code == '204') {
              message.error(JSON.parse(reader.result).message);
            }
          } catch (exception) {
            if (!resp.data) {
              return false;
            } else {
              const blob = new Blob([resp.data], {
                type: contentType,
              });
              const contentDisposition = resp.headers['content-disposition'];
              let fileName = 'unknown';
              if (contentDisposition) {
                fileName = window.decodeURIComponent(
                  window.decodeURI(resp.headers['content-disposition'].split('=')[1]),
                );
              }
              that.downFile(blob, fileName);
            }
          }
        };
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  downFile(blob, fileName) {
    // 非IE下载
    if ('download' in document.createElement('a')) {
      let link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob); // 创建下载的链接
      link.download = fileName; // 下载后文件名
      link.style.display = 'none';
      document.body.appendChild(link);
      link.click(); // 点击下载
      window.URL.revokeObjectURL(link.href); // 释放掉blob对象
      document.body.removeChild(link); // 下载完成移除元素
    } else {
      // IE10+下载
      window.navigator.msSaveBlob(blob, fileName);
    }
  }
  uploadImg(file, val) {
    let header = {};
    if (['image/png', 'image/jpeg', 'image/jpg'].indexOf(file.type) > -1) {
      header = { mime: 'image/jpg' };
    }
    let suffix = val.substr(val.indexOf('.'));
    let storeAs = 'file/' + new Date() * 1 + '/' + val.replace(/[:*|'"!@#$%&<>?\/\\]/g, '');
    return client
      .multipartUpload(storeAs, file, {
        ...header,
      })
      .then((result) => {
        let obj = {};
        obj.key = result.name;
        obj.name = result.name;
        obj.uid = result.name;
        obj.url = client.signatureUrl(result.name);
        return Promise.resolve(obj);
      });
  }
  request(url, options, hasMessage, hasLoading, responseType = 'json') {
    options.headers = this.defaultHeader(); //默认headers
    return service({
      responseType,
      method: options.method,
      url: url,
      data: options.body,
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        authorization: localStorage.getItem('authorization') || '',
      },
      timeout: responseType === 'blob' ? 60000 : 50000,
    })
      .then(function (res) {
        if (res && res.data && res.status === 200) {
          //无权限处理
          if (res.data.code === 1002) {
            document.cookie = 'initUrl=' + '/404';
            history.push('/404');
          }
          // 未登录跳转到首页
          if (res.data.code === 401) {
            removeAllCookie();
            history.push('/home');
            return Promise.reject({ message: '请重新登录' });
          }
          //未认证，不报异常
          if (res.data.code === 3006) {
            return Promise.reject(res.data);
          }
          if (res.data.code === 5000) {
            Modal.error({
              title: '错误详情',
              content: (
                <>
                  <div>错误码：{res.data.data.code}</div>
                  <div style={{ marginTop: 16 }}>requestID：{res.data.data.requestId}</div>
                  <div style={{ marginTop: 8 }}>message：{res.data.data.convertMessage}</div>
                </>
              ),
            });
          }
          if (res.data.code === 303) {
            removeAllCookie();
            history.push('/');
          } else if (res.data.code != 200) {
            return Promise.reject({ res });
          } else {
            if (hasMessage) {
              message.destroy();
              message.success(res.data.message);
            }
            return Promise.resolve(res.data);
          }
        } else if (responseType === 'blob') {
          return Promise.resolve(res);
        } else {
          return Promise.reject({
            message: '服务器返回错误',
          });
        }
      })
      .catch((err) => {
        let data = {};
        // 未登录跳转到首页
        if (err.res && err.res.data && err.res.data.code === 401) {
          removeAllCookie();
          history.push('/home/login');
          return Promise.reject({ message: '请重新登录' });
        }
        if (err.res && err.res.data && err.res.data.message) {
          data = { message: err.res.data.message };
        } else if (err.message) {
          if (responseType === 'blob' && err.message.indexOf('timeout') > -1) {
            message.destroy();
            message.error('网络超时,请稍后再试');
          }
          // 网络超时
          data = { message: err.message };
          // 导出网络超时给出提示
        } else {
          data = { message: '未知错误' };
        }
        message.destroy();
        err.res &&
          err.res.data &&
          err.res.data.code &&
          err.res.data.code !== 5000 &&
          message.error(data.message);
        return Promise.reject(data);
      });
  }

  defaultHeader() {
    // 默认头
    const header = {
      Accept: '*/*',
      'Content-Type': 'application/json',
    };
    return header;
  }

  build(url, params) {
    // URL构建方法
    const ps = [];
    if (params) {
      for (let p in params) {
        if (p) {
          ps.push(p + '=' + encodeURIComponent(params[p]));
        }
      }
    }
    return url + '?' + ps.join('&');
  }

  buildFormData(params) {
    if (params) {
      const data = new FormData();
      for (let p in params) {
        if (p) {
          data.append(p, params[p]);
        }
      }
      return data;
    }
  }
}
export default new Http();
