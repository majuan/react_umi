/**
 * 老的请求方法
 */
import http from './fetch';
const baseUrl = process.env.API_URL ? process.env.API_URL : '';

export default {
 //全局接口
  //列表
  getList(param) {
    return http.post(`${baseUrl}/getList`, param, false);
  },

  //获取菜单栏
  queryForUser(param) {
    return http.post(`${baseUrl}/queryForUser`, param, false);
  },
  //查看页面权限
  checkPermissionByMenuId(param) {
    return http.post(
      `${baseUrl}/checkPermissionByMenuId`,
      param,
      false,
    );
  },

  //修改项目成员
  updateProjectUser(param) {
    return http.post(
      `${baseUrl}/updateProjectUser`,
      param,
      true,
    );
  },

  //移除项目成员
  deleteProjectUser(param) {
    return http.post(
      `${baseUrl}/deleteProjectUser`,
      param,
      true,
    );
  },
  //添加项目成员
  addProjectUser(param) {
    return http.post(
      `${baseUrl}/addProjectUser`,
      param,
      false,
    );
  },

};
