import { parse } from 'querystring';

/* eslint no-useless-escape:0 import/prefer-default-export:0 */
const reg = /(((^https?:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+(?::\d+)?|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)$/;

export const isUrl = (path: string): boolean => reg.test(path);

export const isAntDesignPro = (): boolean => {
  if (ANT_DESIGN_PRO_ONLY_DO_NOT_USE_IN_YOUR_PRODUCTION === 'site') {
    return true;
  }
  return window.location.hostname === 'preview.pro.ant.design';
};

// 给官方演示站点用，用于关闭真实开发环境不需要使用的特性
export const isAntDesignProOrDev = (): boolean => {
  const { NODE_ENV } = process.env;
  if (NODE_ENV === 'development') {
    return true;
  }
  return isAntDesignPro();
};

export const getPageQuery = () => parse(window.location.href.split('?')[1]);

//导出
export const downloadFile = (response, filename) => {
  const blob = new Blob([response], {
    type: response.type,
  });
  if ('download' in document.createElement('a')) {
    const link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob); // 创建下载的链接
    link.download = filename; // 下载后文件名
    link.style.display = 'none';
    document.body.appendChild(link);
    link.click(); // 点击下载
    window.URL.revokeObjectURL(link.href); // 释放掉blob对象
    document.body.removeChild(link); // 下载完成移除元素
  } else {
    // IE10+下载
    window.navigator.msSaveBlob(blob, filename);
  }
};

export const localDb = {
  /**
   * 按key存贮数据value到localStorage
   * @param {String} key   存贮数据的唯一标识
   * @param {String, Object} value 所要存贮的数据
   */
  set(key, value) {
      if (!value) delete window.localStorage[key];
      else {
          const val = typeof value === 'object' ? JSON.stringify(value) : value;
          window.localStorage[key] = val;
      }
  },

  /**
   * 通过key从localStorage获取数据
   * @param  {String} key  获取数据的可以标识
   * @return {String, Object}  返回空，字符串或者对象
   */
  get(key) {
      const str = window.localStorage[key] || '';
      return this.isJSONStr(str) ? JSON.parse(str) : str;
  },

  /**
   * 判断是否是JSON string
   * @param  {String}  str 所要验证的字符串
   * @return {Boolean}   是否是JSON字符串
   */
  isJSONStr(str) {
      return (
          (str.charAt(0) === '{' && str.charAt(str.length - 1) === '}') ||
          (str.charAt(0) === '[' && str.charAt(str.length - 1) === ']')
      );
  },

  // 设置用户信息
  setUserInfo(userInfoKey, data, persistent = false) {
      if (persistent) {
          this.set(userInfoKey, data);
      } else {
          sessionStorage.setItem(userInfoKey, JSON.stringify(data));
      }
  },
  // 获得用户信息
  getUserInfo(userInfoKey, persistent = false) {
      if (persistent) {
          return this.get(userInfoKey);
      } else {
          const str = window.sessionStorage[userInfoKey] || '';
          return this.isJSONStr(str) ? JSON.parse(str) : str;
      }
  },
  /**
   * 清空localStorage
   * @return 无返回NULL
   */
  clear(key) {
      if (key) {
          window.localStorage.removeItem('key');
      } else {
          window.localStorage.clear();
      }
  },
};

// 设置cookie
export const setCookie = (name, value, n) => {
  var oDate = new Date();
  oDate.setDate(oDate.getDate() + n);
  document.cookie = name + '=' + value + ';expires=' + oDate;
};
// 获取cookie 的
export const getCookie = (name) => {
  var str = document.cookie;
  var arr = str.split('; ');
  for (var i = 0; i < arr.length; i++) {
      //console.log(arr[i]);
      var newArr = arr[i].split('=');
      if (newArr[0] == name) {
          return newArr[1];
      }
  }
};
// 删除cookie
export const removeCookie = (name) => {
  setCookie(name, 1, -1);
};

// 千分位分隔符
export const formatMoney = (num) => {
  if (!/^(\+|-)?(\d+)(\.\d+)?$/.test(num)) {
      return num;
  }
  var a = RegExp.$1,
      b = RegExp.$2,
      c = RegExp.$3;
  var re = new RegExp('(\\d)(\\d{3})(,|$)');
  while (re.test(b)) b = b.replace(re, '$1,$2$3');
  return a + '' + b + '' + c;
};

export const downloadFile = (response, filename) => {
  const blob = new Blob([response], {
      type: response.type,
  });
  if ('download' in document.createElement('a')) {
      let link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob); // 创建下载的链接
      link.download = filename; // 下载后文件名
      link.style.display = 'none';
      document.body.appendChild(link);
      link.click(); // 点击下载
      window.URL.revokeObjectURL(link.href); // 释放掉blob对象
      document.body.removeChild(link); // 下载完成移除元素
  } else {
      // IE10+下载
      window.navigator.msSaveBlob(blob, filename);
  }
};

const OSS = window.OSS;
// oss - 内网配置;
const client = new OSS({
  region: '',
  accessKeyId: '',
  accessKeySecret: '',
  secure: true,
  bucket: 'wpop', //自定义的上传后地址，加在oss前
});

export const uploadFile = (file, val) => {
  let suffix = val.substr(val.indexOf('.'));
  let header = {};
  if (['image/png', 'image/jpeg', 'image/jpg'].indexOf(file.type) > -1) {
      header = { mime: 'image/jpg' };
  }
  let storeAs = 'file/' + new Date() * 1 + '/' + val.replace(/[:*|'"!@#$%&<>?\/\\]/g, '');
  return client
      .multipartUpload(storeAs, file, {
          ...header,
      })
      .then((result) => {
          const obj = {};
          obj.key = result.name;
          obj.name = val;
          obj.uid = result.name;
          obj.url = client.signatureUrl(result.name);
          return Promise.resolve(obj);
      });
};

// oss预览
export const ossPreview = (name) => {
  const url = client.signatureUrl(decodeURIComponent(name));
  window.open(url);
};
// oss下载
export const ossDownload = (val, name) => {
  const response = {
      'content-disposition': `attachment; filename=${encodeURIComponent(name)}`,
  };
  const url = client.signatureUrl(val, { response });
  window.open(url);
};