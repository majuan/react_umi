/**
 * 限制小数点前几位 后几位
 * @param {*} //type 类型
 */
 export function decimalPointLimit(type,name,front,after) {
    //${front}${after}
    if(type === "message"){
        return `请输入正确的${name}(小数点前最多${front}位，小数点后最多${after}位)`
    }
    
    return new RegExp(`^[0-9]${front?`{1,${front}}`:'+'}([.][0-9]{1,${after}})?$`)
}

/**
 * 限制小数点前几位 后几位&大于0
 * @param {*} //type 类型
 */
export function positiveFloatingPoint(type,name,front,after) {
    //${front}${after}
    if(type === "message"){
        return `请输入正确的${name}(大于0的浮点数)`
    }
    
    return new RegExp(`^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$`)
}
/**
 * 整整树 且不能大于多少
 * @param {*} //type 类型
 */
export function maxiMum(type,name,front,after) {
    //${max}
    if(type === "message"){
        return `请输入正整数且最大不可超过${front}${after}`
    }
    return new RegExp(`^[0-9]$|^[0-2][0-9]$|^${front}[0-${after}]$`)
}
/**
 * 正整树  BaseInput3
 * @param {*} //type 类型
 */
export function positiveInteger(type,name) {
    //${max}
    if(type === "message"){
        return `请输入正整数`
    }
    return /^[1-9]\d*$/
}
/**
 * 中文+字母+数字
 * @param {*} //type 类型
 */
export function baseInput1(type,name) {
    //${max}
    if(type === "message"){
        return `请输入中文、字母、数字`
    }
    return /^[\u4E00-\u9FA5A-Za-z0-9_]+$/
}
