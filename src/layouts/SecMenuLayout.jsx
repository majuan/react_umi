/**
 * 三级路菜单由配置
 * mj
 */
import React, { useEffect, useRef, useState } from 'react';
import { Link, connect, history } from 'umi';
import { Menu, Layout } from 'antd';
const { Sider, Content } = Layout;
const BasicLayout = (props) => {
  const {
    route: { children, name },
  } = props;
  const [selectKey, setSelectKey] = useState([]);
  useEffect(() => {
    let key = children[0].path;
    children.map((item) => {
      if (!item.hideInSubMenu) {
        if (props.location.pathname.indexOf(item.path) > -1) {
          key = item.path;
        }
      }
    });
    setSelectKey([key]);
  }, [props.location.pathname]);

  return (
    <Layout>
      <h3
        style={{
          position: 'absolute',
          top: '8px',
          marginBottom: 0,
          height: '24px',
          zIndex: 1,
          fontSize: '16px',
          fontWeight: '500',
          fontFamily: 'PingFangSC-Medium',
          color: 'rgba(0,0,0,0.85)',
          lineHeight: '24px',
        }}
      >
        {name || ''}
      </h3>
      <Sider width={200} style={{ background: '#fff', padding: '24px 0', marginRight: 24 }}>
        <span className="line-menu">
          <Menu
            mode="inline"
            selectedKeys={selectKey}
            onSelect={(item) => {
              setSelectKey([item.key]);
            }}
            style={{ height: '100%' }}
          >
            {children &&
              children.map((item, index) => {
                if (!item.hideInSubMenu) {
                  return (
                    <Menu.Item key={item.path}>
                      <Link to={item.path}>{item.name}</Link>
                    </Menu.Item>
                  );
                }
              })}
          </Menu>
        </span>
      </Sider>
      {props.children}
    </Layout>
  );
};

export default connect(({ global, settings }) => ({
  collapsed: global.collapsed,
  settings,
}))(BasicLayout);
