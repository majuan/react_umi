/**
 * 动态菜单
 * mj
 */
import ProLayout, { PageContainer } from '@ant-design/pro-layout';
import React, { useEffect, useState } from 'react';
import { Link, connect, history } from 'umi';
import { Select } from 'antd';
import RightContent from '@/components/GlobalHeader/RightContent';
import Breadcrumb from '@/components/GlobalHeader/Breadcrumb';
import apiService from '@/api/apis';
import '@/iconfont/iconfont.css';
import './BasicLayout.less';
import { cacheData } from '@/utils';
const { Option } = Select;
const BasicLayout = (props) => {
  const {
    dispatch,
    children,
    settings,
    route: { routes },
  } = props;
  const [leftMenu, setLeftMenu] = useState([]);
  const [RegionList, setRegionList] = useState([]); //地域
  const [RegionValue, setRegionValue] = useState(); //地域
  const [menuList, setMenuList] = useState([]); //所有菜单集合
  useEffect(() => {
    getMenu();
    GetRegionList();
  }, []);
  //查看页面权限
  const checkPermission = () => {
    apiService.queryOperateForUserPermission({}).then((res) => {
      if (res && res.code == 200) {
        sessionStorage.setItem('permissionsButtonList', JSON.stringify(res.data));
      }
    });
  };
  //获取下拉地域
  const GetRegionList = () => {
    apiService.GetRegionList({}).then((res) => {
      if (res && res.code === 200) {
        setRegionList(res.data);
        handleRegionChange(res.data[0]);
      }
    });
  };

  //获取后端返回菜单中所有url
  const getUrlList = (list) => {
    let urlList = [];
    const menuList = [];
    list.map((menu) => {
      if (menu.childrenMenuList && menu.childrenMenuList.length > 0) {
        menu.childrenMenuList.map((childMenu) => {
          const { menuUrl } = childMenu;
          urlList.push(menuUrl);
          menuList.push(childMenu);
        });
      } else {
        menuList.push(menu);
        urlList.push(menu.menuUrl);
      }
    });
    setMenuList(menuList);
    urlList.map((item, index) => {
      urlList.push(`/${item.split('/')[1]}`);
    });
    urlList = Array.from(new Set(urlList));
    return urlList;
  };
  //默认跳转到第一个菜单并展开
  const toFirstMenuPage = (item) => {
    let url =
      item.childrenMenuList && item.childrenMenuList.length > 0
        ? item.childrenMenuList[0].menuUrl
        : item.menuUrl;
    history.push(url);
  };
  // 获取后端菜单 isTop: true 设置中心\false 左侧菜单
  const getMenu = () => {
    apiService.queryForUser({}).then((res) => {
      if (res && res.code === 200) {
        if (res.data && res.data[0]) {
          const urlList = getUrlList(res.data);
          let pathname = history.location.pathname.split('?')[0];
          if (pathname == '/workTask' && !urlList.includes(pathname)) {
            toFirstMenuPage(res.data[0]);
          }
          res.data.map((menu, index) => {
            mapResult(menu);
          });
          props.route.routes.map((item) => {
            if (item.hideInMenu == true) {
              res.data.push(item);
            }
          });
          setLeftMenu(res.data);
        } else {
          if (location.pathname !== '/no-auth') {
            history.replace({
              pathname: '/no-auth',
            });
            props.route.routes.map((item) => {
              if (item.hideInMenu == true) {
                res.data.push(item);
              }
            });
            setLeftMenu([]);
          }
        }
      }
    });
  };

  // 格式化菜单
  const mapResult = (v) => {
    v.path = v.menuUrl;
    if (v.path == history.location.pathname) {
      checkPermission();
    }
    v.name = v.menuName;
    v.children = v.childrenMenuList;
    if (v.childrenMenuList && v.childrenMenuList.length > 0) {
      v.childrenMenuList.map((val) => {
        mapResult(val);
      });
    } else {
      props.route.routes.map((item) => {
        if (searchChildren(v.menuUrl, item) != undefined) {
          v.children = searchChildren(v.menuUrl, item);
        }
      });
      return v;
    }
  };
  const searchChildren = (url, item) => {
    let arr = undefined;
    if (item.path == url) {
      arr = item.routes;
    } else {
      if (item.routes) {
        item.routes.map((v, i) => {
          if (v.path == url) {
            arr = v.routes;
          } else {
            if (v.routes) {
              v.routes.map((it) => {
                if (it.path == url) {
                  arr = it.children;
                }
              });
            }
          }
        });
      }
    }
    if (arr != undefined) {
      return arr;
    }
  };
  const handleMenuCollapse = (payload) => {
    if (dispatch) {
      dispatch({
        type: 'global/changeLayoutCollapsed',
        payload,
      });
    }
  };

  const handleRegionChange = (value) => {
    setRegionValue(value);
    cacheData.set('RegionId', value);
    window.RegionId = value;
  };

  return (
    <ProLayout
      {...props}
      {...settings}
      menuExtraRender={() => (
        <>
          <i className="iconfont icon-location-fill" onClick={() => {}}></i>
          <Select
            value={RegionValue}
            style={{ width: '100%', background: 'rgba(0, 0, 0, 0.06)' }}
            bordered={false}
            onChange={handleRegionChange}
          >
            {RegionList.length > 0 &&
              RegionList.map((item) => (
                <Option value={item} key={item}>
                  {item}
                </Option>
              ))}
          </Select>
        </>
      )}
      onCollapse={handleMenuCollapse}
      menuItemRender={(menuItemProps, defaultDom) => {
        if (menuItemProps.isUrl || !menuItemProps.path) {
          return defaultDom;
        }
        return <Link to={menuItemProps.path}>{defaultDom}</Link>;
      }}
      menuDataRender={() => {
        return leftMenu;
      }}
      headerContentRender={(props1) => <Breadcrumb props1={props1} />}
      rightContentRender={() => <RightContent />}
      onPageChange={(location) => {
        var pageMenu = menuList.filter(function (obj) {
          return obj.menuUrl === location.pathname; //获取性别为man的对象
        });
        if (pageMenu.length > 0) {
          checkPermission();
        }
      }}
    >
      {children}
    </ProLayout>
  );
};

export default connect(({ global, settings }) => ({
  collapsed: global.collapsed,
  settings,
}))(BasicLayout);
