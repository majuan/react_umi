/**
 * 超出显示省略号组件
 * 用法：
import EllipsisText from '@/components/renderEllipsisText';
<EllipsisText line={1}>test</EllipsisText>
 * 
 */
import React, { Component } from 'react';
import { Tooltip } from 'antd';
import './index.less';

export default class EllipsisText extends Component {
  static defaultProps = {
    line: 2,
    ellipsis: '...',
  };

  constructor(props) {
    super(props);

    this.showTip = false;
    this.text = '';
  }

  componentDidMount() {
    this.init();
  }

  componentDidUpdate() {
    this.init();
  }

  init = () => {
    if ('webkitLineClamp' in document.documentElement.style) {
      this.setLineClamp();
      this.removeTpl();
    } else {
      this.setLineNormal();
      this.clipText();
    }
  };

  removeTpl = () => {
    try {
      this.refs.ellip.removeChild(this.refs.getHeight);
    } catch (err) {}
  };

  setLineNormal = () => {
    Object.assign(this.refs.ellip.style, {
      'word-break': 'break-all',
      'white-space': 'normal',
    });
  };

  setLineClamp = () => {
    Object.assign(this.refs.ellip.style, {
      overflow: 'hidden',
      display: '-webkit-box',
      webkitBoxOrient: 'vertical',
      'word-break': 'break-all',
      'white-space': 'normal',
      webkitLineClamp: this.props.line,
    });
  };

  clipText = () => {
    let { line, ellipsis, end = () => {} } = this.props;
    let ellip = this.refs.ellip;

    if (!this.h) {
      let getHeight = this.refs.getHeight;
      this.h = getHeight.offsetHeight;
      this.removeTpl();
    }

    let getCountHeight = () => {
      return parseFloat(getComputedStyle(ellip)['height'], 10);
    };

    let init = true;

    if (!this.text) {
      this.text = ellip.textContent;
    } else {
      ellip.innerHTML = this.text;
    }

    let text = this.text;
    let clip = () => {
      let len = 0;
      while (Math.floor(getCountHeight() / this.h) > line) {
        len += 1;

        text = text.slice(0, -1);
        ellip.innerHTML = text;

        if (!init) {
          ellip.innerHTML += ellipsis;
        }
      }

      return len;
    };

    if (0 < clip()) {
      ellip.innerHTML += ellipsis;
      this.showTip = true;
      init = false;
      clip();
    }

    end();
  };

  render() {
    let { children, className = '', lineHeight } = this.props;

    return (
      <Tooltip placement="topLeft" title={children}>
        <div ref="box" className={className}>
          <div ref="ellip" style={{ lineHeight: lineHeight || '24px' }}>
            {children || '-'}
            <span ref="getHeight" style={{ visibility: 'hidden' }}>
              好
            </span>
          </div>
        </div>
      </Tooltip>
    );
  }
}
