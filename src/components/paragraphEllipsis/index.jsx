/**
 * 超出显示省略号组件
 * 用法：
import ParagraphEllipsis from '@/components/paragraphEllipsis';
<ParagraphEllipsis ellipsis={{ rows: 2 }} title="ss、bb、dd">
  <span>测试1：</span>
  ss、bb、dd
</ParagraphEllipsis>
 * 
 */
import React, { useState } from 'react';
import { Typography, Tooltip } from 'antd';
import { withRouter } from 'react-router';
import './index.less';
const { Paragraph } = Typography;

const ParagraphEllipsis = (props) => {
  const [hasEllipsis, setHasEllipsis] = useState(false);
  const {
    children,
    ellipsis = { rows: 1 },
    title,
    tooltipProps = {},
    paragraphProps = {},
    className,
  } = props;
  const [visible, setVisible] = useState(false);
  return (
    <Tooltip
      title={title ? title : children}
      placement="topLeft"
      arrowPointAtCenter
      visible={visible}
      overlayStyle={{ color: 'red' }}
      onVisibleChange={(vis) => {
        setVisible(hasEllipsis ? vis : false);
      }}
      {...tooltipProps}
    >
      <Paragraph
        className={className ? `${className} myParagraphBox` : 'myParagraphBox'}
        style={{ marginBottom: 0, cursor: 'pointer' }}
        ellipsis={{
          rows: 1,
          onEllipsis: function (status) {
            setHasEllipsis(status);
          },
          ...ellipsis,
        }}
        {...paragraphProps}
      >
        {children}
      </Paragraph>
    </Tooltip>
  );
};
export default withRouter(ParagraphEllipsis);
