/*
 * @页面操作权限设置
 * @Date: 2020-08-12 15:59:01
 * @LastEditTime: 2020-09-04 14:26:01
 */
import React from 'react';
import PropTypes from 'prop-types';
import apiService from '@/api/apis';

/**
 * 权限组件封装
 */
export default class AuthWrapper extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      AuthWrapperStatus: false,
    };
  }
  componentDidMount = () => {
    this.checkAuth(this.props.functionName, this.props.menuId);
  };
  /**
   * 校验当前用户是否有功能编码对应的权限
   * @param {string} functionName
   */
  checkAuth = (functionName, menuId) => {
    let flag = false;
    let _this = this;
    let functionsList = [];
    if (sessionStorage.getItem('permissionsButtonList')) {
      functionsList = JSON.parse(sessionStorage.getItem('permissionsButtonList'));
      const functions = functionName.split(',');
      flag = functions.some((value) => functionsList.some((func) => func.menuCode == value.trim()));
      _this.setState({
        AuthWrapperStatus: flag,
      });
    } else {
      // apiService.queryOperateForUserPermission({}).then((res) => {
      //   if (res && res.code == 200) {
      //     if (functionName) {
      //       sessionStorage.setItem('permissionsButtonList', JSON.stringify(res.data));
      //       functionsList = res.data; //[{menuId:0,btnCode:"add"},{menuId:2,btnCode:"del"}]
      //       //这边有一个菜单ID-主要是为了兼容复用同一个组件情况
      //       if (menuId) {
      //         functionsList = functionsList.filter((item) => {
      //           return item.menuId == menuId;
      //         });
      //       }
      //       const functions = functionName.split(',');
      //       flag = functions.some((value) =>
      //         functionsList.some((func) => func.menuCode == value.trim()),
      //       );
      //       console.log(flag);
      //       _this.setState({
      //         AuthWrapperStatus: flag,
      //       });
      //     } else {
      //       flag = false;
      //     }
      //   } else {
      //     flag = false;
      //   }
      // });
    }

    return flag;
  };

  render() {
    return this.state.AuthWrapperStatus == true ? this.props.children : null;
  }
}

AuthWrapper.propTypes = {
  functionName: PropTypes.string,
  menuId: PropTypes.string,
};
