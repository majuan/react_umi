/*
 * @Date: 2021-3-24 14:37:36
 * @LastEditors: majuan
 * @LastEditTime:
 * @desc:
 */

import React from 'react';
import { Upload, Modal, message, Form } from 'antd';
import { PlusOutlined, LeftOutlined, LoadingOutlined } from '@ant-design/icons';
import { withRouter } from 'react-router-dom';
import { uploadFile } from '@/utils/utils';

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}

class PicturesWall extends React.Component {
  constructor(props) {
    super(props);
    this.ref = React.createRef();
    this.state = {
      previewVisible: false,
      previewImage: '',
      fileList: [],
      wrapperFileList: [],
      loading: false,
    };
  }
  // 每次弹窗关闭，重置fileList
  static getDerivedStateFromProps(nextProps, prevState) {
    //该方法内禁止访问this
    if (nextProps.fileList != prevState.wrapperFileList) {
      console.log(nextProps.fileList, '2222');
      // console.log(nextProps.initValue, "nextProps.initValue");
      //通过对比nextProps和prevState，返回一个用于更新状态的对象
      return {
        fileList: nextProps.fileList && nextProps.fileList.length > 0 ? nextProps.fileList : [],
        wrapperFileList: nextProps.fileList,
      };
    }
    //不需要更新状态，返回null
    return null;
  }

  beforeUpload = (file) => {
    const isJpgOrPng =
      file.type === 'image/jpeg' || file.type === 'image/png' || file.type === 'image/jpg';
    if (!isJpgOrPng) {
      message.error('你只能上传JPG,PNG,JPEG文件!');
      return isJpgOrPng;
    }
    const isLt2M = file.size / 1024 / 1024 < 5;
    if (!isLt2M) {
      message.error('图片不能大于5MB!');
    }
    return isLt2M;
  };
  handleCancel = () => this.setState({ previewVisible: false });

  handlePreview = async (file) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }

    this.setState({
      previewImage: file.url || file.preview,
      previewVisible: true,
      previewTitle: file.name || file.url.substring(file.url.lastIndexOf('/') + 1),
    });
  };

  onChange = (file) => {
    if (file.file.status === 'removed') {
      this.setState({
        fileList: file.fileList,
      });
    } else {
      file.fileList[0].status = 'done';
    }
  };

  customRequest = (info) => {
    this.setState({
      loading: true,
    });
    uploadFile(info.file, info.file.name).then((data) => {
      this.setState({
        loading: false,
      });
      const { fileList } = this.state;
      data.status = 'done';
      fileList.push(data);
      console.log(fileList, 'customRequest');
      this.setState(
        {
          fileList: [...fileList],
        },
        () => {},
      );
    });
  };
  normFile = (file) => {
    console.log(file, 'normFile');
    if (file.file.status === 'removed') {
      console.log(file.fileList, 'remove');
      return file.fileList;
    } else {
      return this.state.fileList;
    }
  };

  render() {
    /**
     * @param {nameKey} 上传图片字段名称
     * @param {maxLength} 最多几张图
     */
    const { previewVisible, previewImage, fileList, loading } = this.state;
    const {
      nameKey,
      label,
      extra,
      maxLength,
      disabled, // 禁止图片上传
      showUploadList = true,
      className,
    } = this.props;

    let _length = this.props.form.getFieldValue(nameKey)
      ? this.props.form.getFieldValue(nameKey).length
      : 0;

    const uploadButton = (
      <div>
        {loading ? <LoadingOutlined /> : <PlusOutlined />}
        <div className="ant-upload-text">点击上传图片</div>
      </div>
    );
    console.log(this.props.form.getFieldValue(nameKey), 'name');
    return (
      <div>
        <Form.Item
          name={nameKey}
          label={label}
          rules={[{ required: false, message: `请选择${label}` }]}
          valuePropName="fileList"
          getValueFromEvent={this.normFile}
          extra={extra}
          className="upload-item"
          labelCol={24}
          wrapperCol={24}
          colon={false}
        >
          <Upload
            className={`upload-img ${className ? className : ''}`}
            name="avatar"
            listType="picture-card"
            customRequest={this.customRequest}
            onPreview={this.handlePreview}
            onChange={this.onChange}
            beforeUpload={this.beforeUpload}
            showUploadList={showUploadList}
          >
            {_length >= maxLength || disabled ? null : uploadButton}
          </Upload>
        </Form.Item>

        <Modal
          visible={previewVisible}
          footer={null}
          className="preview-wrapper"
          onCancel={this.handleCancel}
          title={
            <LeftOutlined
              style={{ color: '#999' }}
              onClick={() => {
                this.setState({ previewVisible: false });
              }}
            />
          }
        >
          {previewImage && <img alt="example" style={{ width: '100%' }} src={previewImage} />}
        </Modal>
      </div>
    );
  }
}

export default withRouter(PicturesWall);
