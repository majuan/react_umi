const proSettings = {
  navTheme: 'dark',
  layout: 'side',
  contentWidth: 'Fluid',
  fixedHeader: false,
  fixSiderbar: true,
  colorWeak: false,
  title: '运维管理系统',
  pwa: false,
  iconfontUrl: '//at.alicdn.com/t/font_2225715_igfw6ian1v.js',
  antdGlobalLess: {
    '@primary-color': '#FC860E',
    '@font-size-base': '12px',
    '@line-height-base': '20px',
    '@border-color-base': 'rgba(0, 0, 0, 0.15)', // 边框色
    '@background-color-base': 'rgba(0, 0, 0, 0.04)',
    '@box-shadow-base':
      '0px 12px 48px 16px rgba(0, 0, 0, 0.03), 0px 9px 28px 0px rgba(0, 0, 0, 0.05), 0px 6px 16px -8px rgba(0, 0, 0, 0.08)', // 浮层阴影
    //text
    '@text-color': 'rgba(0, 0, 0, 0.65)',
    //input
    '@input-height-base': '32px',
    '@input-placeholder-color': 'rgba(0, 0, 0, 0.25)',
    '@input-padding-horizontal-base': '8px',
    //modal
    '@modal-header-close-size': '54px',
    '@modal-body-padding': '24px 24px 8px 24px',
    '@modal-footer-padding-vertical': '16px',
    '@modal-footer-padding-horizontal': '24px',
    '@switch-height': '24px',
    '@switch-min-width': '60px',
    //form
    '@form-item-margin-bottom': '16px',
    //button
    '@btn-padding-horizontal-base': '12px',
    //table
    '@table-padding-vertical': '10px',
    //menu
    '@menu-inline-toplevel-item-height': '38px',
    '@menu-item-vertical-margin': '8px',
    '@menu-item-height': '38px',
    '@menu-dark-color': 'rgba(0, 0, 0, 0.85)',
    '@menu-dark-bg': 'rgba(240,240,240)',
    '@menu-dark-arrow-color': 'rgba(0, 0, 0, 0.45)',
    '@menu-dark-submenu-bg': 'rgba(240,240,240)',
    '@menu-dark-item-hover-bg': '@primary-color',
    '@menu-item-font-size': '14px',
    '@message-notice-content-padding': '7px 9px',
    //popover
    '@popover-min-width': '200px',
    '@popover-min-height': '50px',
    '@popover-padding-horizontal': '24px',

    //tabs
    '@tabs-title-font-size': '14px',
    '@tabs-horizontal-margin': '0',
    '@tabs-horizontal-padding': '5px 16px',
  },
};
export default proSettings;
